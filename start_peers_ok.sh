#!/bin/sh
./kill_peers.sh
./peer.py bootstrap &
sleep 2
./peer.py 4002 &
sleep 2

for id in 77 78 79 80
do
	ssh debian@snf-175$id.ok-kno.grnetcloud.net "cd distrib; ./kill_peers.sh; nohup ./peer.py 4001 1>/dev/null 2>/dev/null & sleep 2; nohup ./peer.py 4002 1>/dev/null 2>/dev/null & sleep 2"
done
