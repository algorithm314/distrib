#!/bin/sh

N=10

./peer.py bootstrap &
sleep 0.5

for i in $(seq -f "%02g" 2 10)
do
	./peer.py "40$i" &
	sleep 0.5
done
