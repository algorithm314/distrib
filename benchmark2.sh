#!/bin/bash
# apt install time

d=$(date +%s)

update_configs () {
	sed -ri "s/K_replication = ./K_replication = $1/g" config.py; sed -ri "s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$2'/g" config.py
	for id in 77 78 79 80
	do
		ssh debian@snf-175$id.ok-kno.grnetcloud.net "cd distrib; sed -ri \"s/K_replication = ./K_replication = $1/g\" config.py; sed -ri \"s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$2'/g\" config.py"
	done
}

for K in 1 3 5
do
	for replication in "CHAIN" "EVENTUAL"
	do

		update_configs $K $replication
		./start_peers_ok.sh

		echo $K $replication

		echo "K = $K, Replication = $replication" >> results/insert_$d.txt
		/usr/bin/time sh -c "cat tr_insert.txt | ./cli2.py loop > /dev/null" 2>> results/insert_$d.txt

		echo "YES1"
		echo "K = $K, Replication = $replication" >> results/query_$d.txt
		/usr/bin/time sh -c "cat tr_query.txt | ./cli2.py loop > /dev/null" 2>> results/query_$d.txt

		echo "YES2"
		echo "K = $K, Replication = $replication" >> results/requests_$d.txt
		/usr/bin/time sh -c "cat tr_requests.txt | ./cli2.py loop > /dev/null" 2>> results/requests_$d.txt

		echo "YES3"
		#cat tr_delete.txt | ./cli2.py loop
	done
done
