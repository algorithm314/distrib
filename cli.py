#!/usr/bin/env python3

import sys
from common import *
import json
from config import *
import asyncio
import random

help_msg = '''A simple ToyChord-like Distributed Hash Table.
Agiannis Konstantinos, Kapelonis Eleftherios

usage:
 insert  <key> <value>
 delete  <key>
 query   <key>
 depart
 overlay
 help'''

def get_uri():
	ip = get_ip()
	return f'ws://[{ip}]:{CLI_PORT}'

def error():
	print(help_msg)
	sys.exit(1)

async def get_msg(ws, path):
	resp = await ws.recv()
	print(resp)
	event_loop.stop()

if __name__ == '__main__':
	if len(sys.argv) < 2:
		error()

	ip = get_ip()
	port = CLI_PORT
	start_server = websockets.serve(get_msg, ip, port)		

	event_loop = asyncio.get_event_loop()
	event_loop.run_until_complete(start_server)
	
	if sys.argv[1] == 'insert':
		if len(sys.argv) != 4:
			error()
		d = {
			"query": "insert",
			"key": myhash(sys.argv[2]),
			"value": sys.argv[3],
			"client_uri": get_uri(),
		}

	elif sys.argv[1] == 'query':
		if len(sys.argv) != 3:
			error()
		if sys.argv[2] == "*":
			d = {
				"query": "query_all",
				"client_uri": get_uri(),
			}
		else:
			d = {
				"query": "query",
				"key": myhash(sys.argv[2]),
				"client_uri": get_uri(),
			}

	elif sys.argv[1] == 'delete':
		if len(sys.argv) != 3:
			error()
		d = {
			"query": "delete",
			"key": myhash(sys.argv[2]),
			"client_uri": get_uri(),
		}

	elif sys.argv[1] == 'depart':
		if len(sys.argv) != 3:
			error()
		d = {
			"query": "depart",
			"node_id": sys.argv[2],
			"client_uri": get_uri(),
		}

	elif sys.argv[1] == 'overlay':
		if len(sys.argv) != 2:
			error()
		d = {
			"query": "overlay",
			"client_uri": get_uri(),
		}
		
	elif sys.argv[1] == 'help':
		if len(sys.argv) != 2:
			error()
		print(help_msg)
	else:
		error()

	backend_ip = random.choice(BACKEND_IPS)
	backend_port = random.choice(BACKEND_PORTS)
	client_uri = f'ws://[{backend_ip}]:{backend_port}'
	event_loop.create_task(send_msg(client_uri, json.dumps(d)))
	event_loop.run_forever()


