insert,Like a Rolling Stone,1
insert,Satisfaction,2
insert,Imagine,3
insert,What's Going On,4
insert,Respect,5
insert,Good Vibrations,6
insert,Johnny B. Goode,7
insert,Hey Jude,8
insert,Smells Like Teen Spirit,9
insert,What'd I Say,10
insert,My Generation,11
insert,A Change Is Gonna Come,12
insert,Yesterday,13
insert,Blowin' in the Wind,14
insert,London Calling,15
insert,I Want to Hold Your Hand,16
insert,Purple Haze,17
insert,Maybellene,18
insert,Hound Dog,19
insert,Let It Be,20
insert,Born to Run,21
insert,Be My Baby,22
insert,In My Life,23
insert,People Get Ready,24
insert,God Only Knows,25
insert,A Day in the Life,26
insert,Layla,27
insert,(Sittin on) the Dock of the Bay,28
insert,Help!,29
insert,I Walk the Line,30
insert,Stairway To Heaven,31
insert,Sympathy for the Devil,32
insert,River Deep - Mountain High,33
insert,You've Lost That Lovin' Feelin',34
insert,Light My Fire,35
insert,One,36
insert,No Woman,37
insert,Gimme Shelter,38
insert,That'll Be the Day,39
insert,Dancing in the Street,40
insert,The Weight,41
insert,Waterloo Sunset,42
insert,Tutti-Frutti,43
insert,Georgia on My Mind,44
insert,Heartbreak Hotel,45
insert,Heroes,46
insert,Bridge Over Troubled Water,47
insert,All Along the Watchtower,48
insert,Hotel California,49
insert,The Tracks of My Tears,50
insert,The Message,51
insert,When Doves Cry,52
insert,Anarchy in the U.K.,53
insert,When a Man Loves a Woman,54
insert,Louie Louie,55
insert,Long Tall Sally,56
insert,Whiter Shade of Pale,57
insert,Billie Jean,58
insert,The Times They Are A-Changin',59
insert,Let's Stay Together,60
insert,Whole Lotta Shakin' Goin On,61
insert,Bo Diddley,62
insert,For What It's Worth,63
insert,She Loves You,64
insert,Sunshine of Your Love,65
insert,Redemption Song,66
insert,Jailhouse Rock,67
insert,Tangled Up in Blue,68
insert,Crying,69
insert,Walk On By,70
insert,California Girls,71
insert,Papa's Got a Brand New Bag,72
insert,Summertime Blues,73
insert,Superstition,74
insert,Whole Lotta Love,75
insert,Strawberry Fields Forever,76
insert,Mystery Train,77
insert,I Got You (I Feel Good),78
insert,Mr. Tambourine Man,79
insert,I Heard It Through the Grapevine,80
insert,Blueberry Hill,81
insert,You Really Got Me,82
insert,Norwegian Wood (This Bird Has Flown),83
insert,Every Breath You Take,84
insert,Crazy,85
insert,Thunder Road,86
insert,Ring of Fire,87
insert,My Girl,88
insert,California Dreamin',89
insert,In the Still of the Nite,90
insert,Suspicious Minds,91
insert,Blitzkrieg Bop,92
insert,I Still Haven't Found What I'm Looking For,93
insert,Good Golly,94
insert,Blue Suede Shoes,95
insert,Great Balls of Fire,96
insert,Roll Over Beethoven,97
insert,Love and Happiness,98
insert,Fortunate Son,99
insert,You Can't Always Get What You Want,100
insert,Voodoo Child (Slight Return),101
insert,Be-Bop-A-Lula,102
insert,Hot Stuff,103
insert,Living for the City,104
insert,The Boxer,105
insert,Mr. Tambourine Man,106
insert,Not Fade Away,107
insert,Little Red Corvette,108
insert,Brown Eyed Girl,109
insert,I've Been Loving You Too Long (to Stop Now),110
insert,I'm So Lonesome I Could Cry,111
insert,That's All Right,112
insert,Up on the Roof,113
insert,Da Doo Ron Ron ),114
insert,You Send Me,115
insert,Honky Tonk Women,116
insert,Take Me to the River,  117
insert,Shout (Parts 1 and 2)],118
insert,Go Your Own Way,119
insert,I Want You Back,120
insert,Stand By Me,121
insert,House of the Rising Sun,122
insert,Will You Love Me Tomorrow,123
insert,Shake,124
insert,Changes,125
insert,Rock & Roll Music,126
insert,Born to Be Wild,127
insert,Maggie May,128
insert,With or Without You,129
insert,Who Do You Love,130
insert,Won't Get Fooled Again,131
insert,In the Midnight Hour,132
insert,While My Guitar Gently Weeps,133
insert,Your Song,134
insert,Eleanor Rigby,135
insert,Family Affair,136
insert,I Saw Her Standing There,137
insert,Kashmir,138
insert,All I Have to Do Is Dream,139
insert,Please,140
insert,Purple Rain,141
insert,I Wanna Be Sedated,142
insert,Everyday People,143
insert,Rock Lobster,144
insert,Lust for Life,145
insert,Me and Bobby McGee,146
insert,Cathy's Clown,147
insert,Eight Miles High,148
insert,Earth Angel,149
insert,Foxey Lady,150
insert,A Hard Day's Night,151
insert,Rave On,152
insert,Proud Mary,153
insert,The Sounds of Silence,154
insert,I Only Have Eyes for You,155
insert,(We're Gonna) Rock Around the Clock,156
insert,I'm Waiting for the Man,157
insert,Bring the Noise,158
insert,I Can't Stop Loving You,159
insert,Nothing Compares 2 U,160
insert,Bohemian Rhapsody,161
insert,Folsom Prison Blues,162
insert,Fast Car,163
insert,Lose Yourself,  164
insert,Let's Get It On,165
insert,Papa Was a Rollin' Stone,166
insert,Losing My Religion,167
insert,Both Sides Now,168
insert,Dancing Queen,169
insert,Dream On,170
insert,God Save the Queen,171
insert,Paint It,172
insert,I Fought the Law,173
insert,Don't Worry Baby,174
insert,Free Fallin',175
insert,September Gurls,176
insert,Love Will Tear Us Apart,177
insert,Hey Ya!,178
insert,Green Onions,179
insert,Save the Last Dance for Me,180
insert,The Thrill Is Gone,181
insert,Please Please Me,182
insert,Desolation Row,183
insert,I Never Loved a Man (The Way I Love You),184
insert,Back in Black,185
insert,Who'll Stop the Rain,186
insert,Stayin' Alive,187
insert,Knocking on Heaven's Door,188
insert,Free Bird,189
insert,Wichita Lineman,190
insert,There Goes My Baby,191
insert,Peggy Sue,192
insert,Maybe,193
insert,Sweet Child O' Mine,194
insert,Don't Be Cruel,195
insert,Hey Joe,196
insert,Flash Light,197
insert,Loser,198
insert,Bizarre Love Triangle,199
insert,Come Together,200
insert,Positively 4th Street,201
insert,Try a Little Tenderness,202
insert,Lean On Me,203
insert,Reach Out,204
insert,Bye Bye Love,205
insert,Gloria,206
insert,In My Room,207
insert,96 Tears,208
insert,Caroline,209
insert,1999,210
insert,Your Cheatin' Heart,211
insert,Rockin' in the Free World,212
insert,Sh-Boom,213
insert,Do You Believe in Magic,214
insert,Jolene,215
insert,Boom Boom,216
insert,Spoonful,217
insert,Walk Away Renee,218
insert,Walk on the Wild Side,219
insert,Oh,220
insert,Dance to the Music,221
insert,Good Times,222
insert,Hoochie Coochie Man,223
insert,Moondance,224
insert,Fire and Rain,225
insert,Should I Stay or Should I Go,226
insert,Mannish Boy,227
insert,Just Like a Woman,228
insert,Sexual Healing,229
insert,Only the Lonely,230
insert,We Gotta Get Out of This Place,231
insert,I'll Feel a Whole Lot Better,232
insert,I Got a Woman,233
insert,Everyday,234
insert,Planet Rock,235
insert,I Fall to Pieces,236
insert,The Wanderer,237
insert,Son of a Preacher Man,238
insert,Stand!,239
insert,Rocket Man,240
insert,Love Shack,241
insert,Gimme Some Lovin',242
insert,The Night They Drove Old Dixie Down,243
insert,(Your Love Keeps Lifting Me) Higher and Higher,244
insert,Hot Fun in the Summertime,245
insert,Rappers Delight,246
insert,Chain of Fools,247
insert,Paranoid,248
insert,Mack the Knife,249
insert,Money Honey,250
insert,All the Young Dudes,251
insert,Highway to Hell,252
insert,Heart of Glass,253
insert,Paranoid Android,254
insert,Wild Thing,255
insert,I Can See for Miles,256
insert,Hallelujah,257
insert,Oh,258
insert,Higher Ground,259
insert,Ooo Baby Baby,260
insert,He's a Rebel,261
insert,Sail Away,262
insert,Tighten Up,263
insert,Walking in the Rain,264
insert,Personality Crisis,265
insert,Sunday Bloody Sunday,266
insert,Roadrunner,267
insert,He Stopped Loving Her Today,268
insert,Sloop John B,269
insert,Sweet Little Sixteen,270
insert,Something,271
insert,Somebody to Love,272
insert,Born in the U.S.A.,273
insert,I'll Take You There,274
insert,Ziggy Stardust,275
insert,Pictures of You,276
insert,Chapel of Love,277
insert,Ain't No Sunshine,278
insert,You Are the Sunshine of My Life,279
insert,Help Me,280
insert,Call Me,281
insert,(What's So Funny 'Bout) Peace Love and Understanding?,282
insert,Attractions,283
insert,Smoke Stack Lightning,284
insert,Summer Babe,285
insert,Walk This Way,286
insert,Money (That's What I Want),287
insert,Can't Buy Me Love,288
insert,Stan,289
insert,She's Not There,290
insert,Train in Vain,291
insert,Tired of Being Alone,292
insert,Black Dog,293
insert,Street Fighting Man,294
insert,Get Up,295
insert,Heart of Gold,296
insert,One Way or Another,297
insert,Sign 'O' the Times,298
insert,Like a Prayer,299
insert,Do Ya Think I'm Sexy?,300
insert,Blue Eyes Crying In the Rain,301
insert,Ruby Tuesday,302
insert,With a Little Help From My Friends,303
insert,Say It Loud -- I'm Black and Proud,304
insert,That's Entertainment,305
insert,Why Do Fools Fall In Love,306
insert,Lonely Teardrops,307
insert,What's Love Got To Do With It,308
insert,Iron Man,309
insert,Wake Up Little Susie,310
insert,In Dreams,311
insert,I Put a Spell on You,312
insert,Comfortably Numb,313
insert,Don't Let Me Be Misunderstood,314
insert,Wish You Were Here,315
insert,Many Rivers to Cross,316
insert,Alison,317
insert,School's Out,318
insert,Heartbreaker,319
insert,Cortez the Killer,320
insert,Fight the Power,321
insert,Dancing Barefoot,322
insert,Baby Love,323
insert,Good Lovin',324
insert,Get Up (I Feel Like Being a) Sex Machine,325
insert,For Your Precious Love,326
insert,The End,327
insert,That's the Way of the World,328
insert,We Will Rock You,329
insert,I Can't Make You Love Me,330
insert,Subterranean Homesick Blues,331
insert,Spirit in the Sky,332
insert,Wild Horses,333
insert,Sweet Jane,334
insert,Walk This Way,335
insert,Beat It,336
insert,Maybe I'm Amazed,337
insert,You Keep Me Hanging On,338
insert,Baba O'Riley,339
insert,The Harder They Come,340
insert,Runaround Sue,341
insert,Jim Dandy,342
insert,Piece of My Heart,343
insert,La Bamba,344
insert,California Love,345
insert,Candle in the Wind,346
insert,That Lady (Part 1 and 2),347
insert,Spanish Harlem,348
insert,The Locomotion,349
insert,The Great Pretender,350
insert,All Shook Up,351
insert,Tears in Heaven,352
insert,Watching the Detectives,353
insert,Bad Moon Rising,354
insert,Sweet Dreams (Are Made of This),355
insert,Little Wing,356
insert,Nowhere to Run,357
insert,Got My Mojo Working,358
insert,Killing Me Softly With His Song,359
insert,Complete Control,360
insert,All You Need Is Love,361
insert,The Letter,362
insert,Highway 61 Revisited,363
insert,Unchained Melody,364
insert,How Deep Is Your Love,365
insert,White Room,366
insert,Personal Jesus,367
insert,I'm A Man,368
insert,The Wind Cries Mary,369
insert,I Can't Explain,370
insert,Marquee Moon,371
insert,Wonderful World,372
insert,Brown Eyed Handsome Man,373
insert,Another Brick in the Wall Part 2,374
insert,Fake Plastic Trees,375
insert,Hit the Road Jack,376
insert,Pride (In The Name of Love),377
insert,Radio Free Europe,378
insert,Goodbye Yellow Brick Road,379
insert,Tell It Like It Is,380
insert,Bitter Sweet Symphony,381
insert,Whipping Post,382
insert,Ticket to Ride,383
insert,Ohio,384
insert,I Know You Got Soul,385
insert,Tiny Dancer,386
insert,Roxanne,387
insert,Just My Imagination,388
insert,Baby I Need Your Loving,389
insert,Band of Gold,390
insert,O-o-h Child,391
insert,Summer in the City,392
insert,Can't Help Falling in Love,393
insert,Remember (Walkin' in the Sand),394
insert,Thirteen,395
insert,(Don't Fear) the Reaper,396
insert,Sweet Home Alabama,397
insert,Enter Sandman,398
insert,Kicks,399
insert,Tonight's the Night,400
insert,Thank You (Falettinme Be Mice Elf Agin),401
insert,C'mon Everybody,402
insert,Visions of Johanna,403
insert,We've Only Just Begun,404
insert,I Believe I Can Fly,405
insert,In Bloom,406
insert,Sweet Emotion,407
insert,Crossroads,408
insert,Monkey Gone to Heaven,409
insert,I Feel Love,410
insert,Ode to Billie Joe,411
insert,The Girl Can't Help It,412
insert,Young Blood,413
insert,I Can't Help Myself,414
insert,The Boys of Summer,415
insert,Fuck tha Police,416
insert,Suite: Judy Blue Eyes,417
insert,Nuthin' But a 'G' Thang,418
insert,It's Your Thing,419
insert,Piano Man,420
insert,Lola,421
insert,Blue Suede Shoes,422
insert,Tumbling Dice,423
insert,William,424
insert,Smoke on the Water,425
insert,New Year's Day,426
insert,Devil With a Blue Dress On/Good Golly Miss Molly,427
insert,Wheels,428
insert,Everybody Needs Somebody to Love,429
insert,White Man in Hammersmith Palais,430
insert,Ain't It a Shame,431
insert,Midnight Train to Georgia,432
insert,Ramble On,433
insert,Mustang Sally,434
insert,Beast of Burden,435
insert,Alone Again Or,436
insert,Love Me Tender,437
insert,I Wanna Be Your Dog,438
insert,Pink Houses,439
insert,Push It,440
insert,Come Go With Me,441
insert,Keep a Knockin',442
insert,I Shot the Sheriff,443
insert,I Got You Babe,444
insert,Come As You Are,445
insert,Pressure Drop,446
insert,Leader of the Pack,447
insert,Heroin,448
insert,Penny Lane,449
insert,By the Time I Get to Phoenix,450
insert,The Twist,451
insert,Cupid,452
insert,Paradise City,453
insert,My Sweet Lord,454
insert,All Apologies,455
insert,Stagger Lee,456
insert,Sheena Is a Punk Rocker,457
insert,Soul Man,458
insert,Rollin' Stone,459
insert,One Fine Day,460
insert,Kiss,461
insert,Respect Yourself,462
insert,Rain,463
insert,Standing in the Shadows of Love,464
insert,Surrender,465
insert,Runaway,466
insert,Welcome to the Jungle,467
insert,Search and Destroy,468
insert,It's Too Late,469
insert,Free Man in Paris,470
insert,On the Road Again,471
insert,Where Did Our Love Go,472
insert,Do Right Woman,473
insert,One Nation Under a Groove,474
insert,Sabotage,475
insert,I Want to Know What Love Is,476
insert,Super Freak,477
insert,White Rabbit,478
insert,Lady Marmalade,479
insert,Into the Mystic,480
insert,Young Americans,481
insert,I'm Eighteen,482
insert,Just Like Heaven,483
insert,I Love Rock 'N Roll,484
insert,Graceland,485
insert,How Soon Is Now?,486
insert,Under the Boardwalk,487
insert,Rhiannon (Will You Ever Win),488
insert,I Will Survive,489
insert,Brown Sugar,490
insert,You Don't Have to Say You Love Me,491
insert,Running on Empty,492
insert,Then He Kissed Me,493
insert,Desperado,494
insert,Shop Around,495
insert,Miss You,496
insert,Buddy Holly,497
insert,Rainy Night in Georgia,498
insert,The Boys Are Back in Town,499
insert,More Than a Feeling,500
