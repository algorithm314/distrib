#!/bin/bash
# apt install time

d=$(date +%s)
for K in 1 3 5
do
	for replication in "CHAIN" "EVENTUAL"
	do
		echo "K = $K, Replication = $replication" >> results/insert_$d.txt
		sed -ri "s/K_replication = ./K_replication = $K/g" config.py

		sed -ri "s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$replication'/g" config.py

		/usr/bin/time awk -F "," '{ print("insert \"" $1 "\" " $2) }' transactions/insert.txt > tr_insert.txt
	done
done

for K in 1 3 5
do
	for replication in "CHAIN" "EVENTUAL"
	do
		echo "K = $K, Replication = $replication" >> results/query_$d.txt
		sed -ri "s/K_replication = ./K_replication = $K/g" config.py

		sed -ri "s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$replication'/g" config.py

		/usr/bin/time awk -F "," '{ print("query \"" $1 "\"") }' transactions/query.txt > tr_query.txt
	done
done

for K in 1 3 5
do
	for replication in "CHAIN" "EVENTUAL"
	do
		echo "K = $K, Replication = $replication" >> results/requests_$d.txt
		sed -ri "s/K_replication = ./K_replication = $K/g" config.py

		sed -ri "s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$replication'/g" config.py

		/usr/bin/time awk -F "," '{ if($1 == "insert") print("insert \"" $2 "\" " $3); else print("query \"" $2 "\"")}' transactions/requests2.txt > tr_requests.txt
	done
done

sed -ri 's/ "/,/g;s/"  ?/,/g;s/"//g' tr_insert.txt
sed -ri 's/ "/,/g;s/"  ?/,/g;s/"//g' tr_query.txt
sed -ri 's/ "/,/g;s/"  ?/,/g;s/"//g' tr_requests.txt
