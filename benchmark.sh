#!/bin/bash
# apt install time

d=$(date +%s)
for K in 1 3 5
do
	for replication in "CHAIN" "EVENTUAL"
	do
		echo "K = $K, Replication = $replication" >> results/insert_$d.txt
		sed -ri "s/K_replication = ./K_replication = $K/g" config.py

		sed -ri "s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$replication'/g" config.py

		/usr/bin/time awk -F "," '{ system("./cli.py insert \"" $1 "\" " $2) }' transactions/insert.txt > /dev/null 2>> results/insert_$d.txt
	done
done

for K in 1 3 5
do
	for replication in "CHAIN" "EVENTUAL"
	do
		echo "K = $K, Replication = $replication" >> results/query_$d.txt
		sed -ri "s/K_replication = ./K_replication = $K/g" config.py

		sed -ri "s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$replication'/g" config.py

		/usr/bin/time awk -F "," '{ system("./cli.py query \"" $1 "\"") }' transactions/query.txt > /dev/null 2>> results/query_$d.txt
	done
done

for K in 1 3 5
do
	for replication in "CHAIN" "EVENTUAL"
	do
		echo "K = $K, Replication = $replication" >> results/requests_$d.txt
		sed -ri "s/K_replication = ./K_replication = $K/g" config.py

		sed -ri "s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$replication'/g" config.py

		/usr/bin/time awk -F "," '{ if($1 == "insert") system("./cli.py insert \"" $2 "\" " $3); else system("./cli.py query \"" $2 "\"")}' transactions/requests.txt > /dev/null 2>> results/requests_$d.txt
	done
done
