BOOTSTRAP_IP = '2001:648:2ffe:501:cc00:10ff:fe5e:4d6a'
BOOTSTRAP_PORT = 4001
BOOTSTRAP_URI = f'ws://[{BOOTSTRAP_IP}]:{BOOTSTRAP_PORT}'

BACKEND_IPS = [
	'2001:648:2ffe:501:cc00:10ff:fe5e:4d6a',
	'2001:648:2ffe:501:cc00:12ff:fe54:a1e1',
	'2001:648:2ffe:501:cc00:11ff:fe01:dcab',
	'2001:648:2ffe:501:cc00:10ff:fee6:9e8a',
	'2001:648:2ffe:501:cc00:12ff:fe6a:1ebb',
]
BACKEND_PORTS = [
	4001,
	4002,
]

CLI_PORT = 4000
K_replication = 3
REPLICATION_TYPE = 'CHAIN'
