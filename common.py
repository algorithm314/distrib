import asyncio
import websockets
import hashlib
import socket

async def send_msg(uri, msg):
	async with websockets.connect(uri) as websocket:
		await websocket.send(msg)

def get_ip():
	s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
	try:
		# doesn't even have to be reachable
		s.connect(('2001:648:2ffe:501:cc00:10ff:fe5e:4d6a', 1))
		IP = s.getsockname()[0]
	except Exception:
		IP = '::1'
	finally:
		s.close()
	return IP

def escape(s):
	return s.replace('"','\\"').replace('\\','\\\\')

def myhash(s):
	return int(hashlib.sha1(s.encode('utf-8')).hexdigest(), 16)

def inside_range(key, range_from, range_to):
	if range_from <= range_to:
		return key <= range_to and key >= range_from
	else:
		return key <= range_to or key >= range_from
