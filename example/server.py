
import asyncio
import websockets

name1 = "hello"

async def hello(websocket, path):
    global name1

    name = await websocket.recv()
    print(f"< {name}")

    greeting = f"Hello {name1}!"
    name1 = name
    await websocket.send(greeting)
    print(f"> {greeting}")

start_server = websockets.serve(hello, "localhost", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
