import asyncio

lock = asyncio.Lock()

count = 0

async def handle_echo(reader, writer):

    global count

    data = await reader.read(100)
    message = data.decode()
    addr = writer.get_extra_info('peername')

    print(f"Received {message!r} from {addr!r}")

    print(f"Send: {message!r}")

    # this lock is probably not needed
    # but there are cases it may be needed
    async with lock:
        writer.write(f'{data!r}, count = {count}'.encode())
        count += 1

    await writer.drain()

    print("Close the connection")
    writer.close()

async def main():
    server = await asyncio.start_server(
        handle_echo, '127.0.0.1', 8888)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()

asyncio.run(main())
