import requests
from getpass import getpass
import sys
import asyncio
import websockets
import json
import aioconsole

# Websocket
async def send_msg(ws):
    stdin, _ = await aioconsole.get_standard_streams()
    while True:
        # input from keyboard (not needed here)
        line = await stdin.read(64)
        await ws.send(json.dumps({'receiver': to_id, 'text': line.decode('utf-8').strip()}))

async def get_msg(ws):
    while True:
        # receive (no buffers needed)
        msg = await ws.recv()
        # afou paralaboume ena minima logika blokaroume opote den exoume thema me locks nomizw??
        msg = json.loads(msg)
        print(f"({to_email}):{msg['text']}")


async def all():
    uri = "ws://localhost:4000"
    # nomizw edw de mas xreiazetai auto
    async with websockets.connect(uri, extra_headers=[('authorization', f'Bearer {token}')]) as websocket:
        await asyncio.gather(send_msg(websocket), get_msg(websocket))

asyncio.run(all())
