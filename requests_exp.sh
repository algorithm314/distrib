#!/bin/bash
# apt install time

d=$(date +%s)

update_configs () {
	sed -ri "s/K_replication = ./K_replication = $1/g" config.py; sed -ri "s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$2'/g" config.py
	for id in 77 78 79 80
	do
		ssh debian@snf-175$id.ok-kno.grnetcloud.net "cd distrib; sed -ri \"s/K_replication = ./K_replication = $1/g\" config.py; sed -ri \"s/REPLICATION_TYPE = '[A-Z]+'/REPLICATION_TYPE = '$2'/g\" config.py"
	done
}

K=3
for replication in "CHAIN" "EVENTUAL"
do

	update_configs $K $replication
	./start_peers_ok.sh

	echo $K $replication

	echo "K = $K, Replication = $replication" >> /dev/null
	/usr/bin/time sh -c "cat tr_insert.txt | ./cli2.py loop" 2>> /dev/null
	
	echo "YES1"
	echo "K = $K, Replication = $replication" >> /dev/null
	sleep 5
	/usr/bin/time sh -c "cat tr_query.txt | ./cli2.py loop" 2>> /dev/null

	echo "YES2"
	echo "K = $K, Replication = $replication" >> /dev/null
	sleep 5
	/usr/bin/time sh -c "cat tr_requests.txt | ./cli2.py loop > results/output_requests_$replication.txt" 2>> /dev/null

	echo "YES3"
	#cat tr_delete.txt | ./cli2.py loop
done
