import matplotlib.pyplot as plt
import glob
import re
import time

for filename in glob.glob('results/*.txt'):
    experiment_name = None
    for x in ['insert', 'query']:
        if x in filename:
            experiment_name = x
    if experiment_name is None: continue
    
    xs = {'CHAIN': [], 'EVENTUAL': []}
    ys = {'CHAIN': [], 'EVENTUAL': []}
    with open(filename) as f:
        for line in f:
            if line.startswith('K = '):
                k, consistency = [pair.split('=')[1].strip() for pair in line.split(',')]
            elif 'elapsed' in line:
                r = re.findall(r' (\S+)elapsed', line)[0]
                mins, secs = [float(x) for x in r.split(':')]
                total_secs = mins*60 + secs
                print(total_secs)
                xs[consistency].append(k)
                ys[consistency].append(total_secs)

    fig, ax = plt.subplots()
    for consistency in ['EVENTUAL', 'CHAIN']:
        ax.plot(xs[consistency], ys[consistency], label=consistency)
    ax.legend()
    ax.set_xlabel('k')
    ax.set_ylim(bottom=0, top=20)
    ax.set_ylabel('time (seconds)')
    ax.set_title(experiment_name)
    plt.savefig(f'report/{experiment_name}')
