#!/usr/bin/env python3
# usage ./peer.py bootstrap|port

import asyncio
import websockets
import json
import sys
from common import *
from config import *

prev_peer = None
next_peer = None

# {"key": (value,K)}

pairs = {}

# json format
# 'query': insert,query,delete
# 'key'
# 'value' # optional

# Peer is a dict with keys ip, port

# REPLICATION
# Each key stored in pairs has a number associated related to replication
# This number (replication) is in the range 1...K_replication
# K_replication means that this is the primary node
# K_replication - 1 means that this is the second node etc
# 1 means that this is the last node

def get_uri(peer):
	if peer == None:
		print('ERROR: You cant send this message. The DHT is probably empty')
	return f'ws://[{peer["ip"]}]:{peer["port"]}'

def get_hash(peer):
	return myhash(f'[{peer["ip"]}]:{peer["port"]}')

async def handle_req_locally(request_dict):
	key = request_dict['key']
	query = request_dict['query']
	client_uri = request_dict['client_uri']
	if query in ['insert', 'delete']:
		replication = request_dict['replication']

		if query == 'insert':
			val = request_dict['value']
			pairs[key] = [val, replication]
		else: # delete
			pairs.pop(key, None)

		# for eventual the first node notifies client
		if REPLICATION_TYPE == 'EVENTUAL' and replication == K_replication:
			response = json.dumps({})
			await send_msg(client_uri, response)

		replication -= 1
		request_dict['replication'] = replication

		if replication > 0:
			await send_msg(get_uri(next_peer), json.dumps(request_dict))
			return

		# for chain replication the last node notifies client
		if REPLICATION_TYPE == 'CHAIN':
			response = json.dumps({})
			await send_msg(client_uri, response)
		return
	
	elif query == 'query':
		if key in pairs:
			if REPLICATION_TYPE == 'CHAIN' and pairs[key][1] != 1:
				request_dict['replication'] -= 1
				await send_msg(get_uri(next_peer), json.dumps(request_dict))
				return
			else:
				response = json.dumps({'value': pairs[key][0]})
		else:
			if request_dict['replication'] == 1:
				response = json.dumps({'error': 'Key does not exist'})
			else:
				request_dict['replication'] -= 1
				response = json.dumps(request_dict)
				await send_msg(get_uri(next_peer), json.dumps(request_dict))
				return
	else:
		print('ERROR')

	await send_msg(client_uri, response)

def print_configuration():
	print(f'pairs={pairs}')
	print(f'range_from={range_from}')
	print(f'range_to={range_to}')
	print(f'id={get_hash(me)}')
	print(f'prev_peer={prev_peer["port"]}')
	print(f'next_peer={next_peer["port"]}')
	print()

async def handle_req(ws, path):

	# global state
	global prev_peer, next_peer, range_from, range_to, pairs, port
	##############
	request_text = await ws.recv()
	#print(get_uri(me))
	#print(request_text)
	request_dict = json.loads(request_text)
	query_type = request_dict['query']

	# for insert and delete need to find first node
	# for query and chain replication the last
	# for query and eventual any
	if query_type in ['insert', 'delete', 'query']:
		key = request_dict['key']
		if 'replication' not in request_dict:
			request_dict['replication'] = K_replication
		if inside_range(key, range_from, range_to) or request_dict['replication'] < K_replication:
			await handle_req_locally(request_dict)
		else:
			await send_msg(get_uri(next_peer), request_text)

	# This is needed for join
	elif query_type == 'decrease':
		key = request_dict['key']
		if key in pairs:
			if pairs[key][1] > 1:
				pairs[key][1] -= 1
				await send_msg(get_uri(next_peer), request_text)
			else:
				pairs.pop(key, None)

	# This is needed for depart
	elif query_type == 'increase':
		key = request_dict['key']
		if key in pairs:
			if pairs[key][1] < K_replication:
				pairs[key][1] += 1
				await send_msg(get_uri(next_peer), request_text)
		else:
			# I am the last one; we are done
			value = request_dict['value']
			pairs[key] = [value, 1]

	elif query_type == 'join':
		new_peer = request_dict['new_peer']
		new_peer_id = get_hash(new_peer)

		# print(key, range_from, range_to)

		if inside_range(new_peer_id, range_from, range_to):
			# Calculate pairs to send to new_peer
			new_peer_pairs = {}
			my_new_pairs = {}

			for key in pairs.copy():
				if inside_range(key, range_from, new_peer_id) or pairs[key][1] < K_replication:
					new_peer_pairs[key] = pairs[key]
					resp = json.dumps({'query': 'decrease', 'key': key})
					# send to myself first
					await send_msg(get_uri(me), resp)
				else:
					my_new_pairs[key] = pairs[key]

			# Notify prev_peer to update its values
			if prev_peer is None:
				prev_peer = me
			else:
				resp = json.dumps({'query': 'update', 'next_peer': new_peer})
				await send_msg(get_uri(prev_peer), resp)

			# Notify new_peer to update (initialize) its values
			resp = json.dumps({'query': 'update', 'pairs': new_peer_pairs, 'prev_peer': prev_peer, 'next_peer': me})
			await send_msg(get_uri(new_peer), resp)

			# Update my values
			pairs = my_new_pairs
			prev_peer = new_peer
			if next_peer is None:
				next_peer = new_peer
			range_from = get_hash(prev_peer)
			range_to = get_hash(me)

			# print_configuration()
		else:
			await send_msg(get_uri(next_peer), request_text)

	# The node_id MUST exist
	elif query_type == 'depart':
		node_id = int(request_dict['node_id'])
		if node_id == get_hash(me):
			await send_msg(get_uri(prev_peer), json.dumps({
				'query': 'update', 'next_peer': next_peer
			}))

			await send_msg(get_uri(next_peer), json.dumps({
				'query': 'update', 'prev_peer': prev_peer
			}))

			for key in pairs:
				await send_msg(get_uri(next_peer), json.dumps({
					'query': 'increase', 'key': key, 'value': pairs[key][0]
				}))

			# Die :(
			client_uri = request_dict['client_uri']
			await send_msg(client_uri, json.dumps({})) 
			event_loop.stop()
		else:
			await send_msg(get_uri(next_peer), request_text)

	elif query_type == 'update':
		# Get my configuration
		if 'pairs' in request_dict:
			pairs = request_dict['pairs']
			# json converts int keys to string keys
			# as only string keys are allowed
			# convert them back
			pairs = {int(key):value for key,value in pairs.items()}
		if 'prev_peer' in request_dict:
			prev_peer = request_dict['prev_peer']
			if prev_peer == me:
				prev_peer = None
			range_from = get_hash(prev_peer)
		if 'next_peer' in request_dict:
			next_peer = request_dict['next_peer']
			if next_peer == me:
				next_peer = None
		range_to = get_hash(me) # kalou kakou (kanonika tha prepei na einai stathero)
		# print_configuration()

	elif query_type == 'overlay':
		if 'network' not in request_dict:
			request_dict['network'] = []
		request_dict['network'].append((get_hash(me), get_uri(me)))
		
		print_configuration()

		if next_peer is not None and get_hash(next_peer) != request_dict['network'][0][0]:
			await send_msg(get_uri(next_peer), json.dumps(request_dict))
		else:
			client_uri = request_dict['client_uri']
			list_of_strings = [f'{uri}, {idd}' for idd, uri in request_dict['network']]
			await send_msg(client_uri, '\n'.join(list_of_strings))
	
	elif query_type == 'query_all':
		if 'pairs' not in request_dict:
			request_dict['pairs'] = []
		request_dict['pairs'].append((get_hash(me), pairs))

		if next_peer is not None and get_hash(next_peer) != request_dict['pairs'][0][0]:
			await send_msg(get_uri(next_peer), json.dumps(request_dict))
		else:
			client_uri = request_dict['client_uri']
			list_of_strings = [f'Node ID {idd}: {pairs}' for idd, pairs in request_dict['pairs']]
			await send_msg(client_uri, '\n'.join(list_of_strings))

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print('usage: {sys.argv[0]} bootstrap|port')
		sys.exit(1)

	# Bootstrap node
	if sys.argv[1] == 'bootstrap': 
		me = {
			'ip': BOOTSTRAP_IP,
			'port': BOOTSTRAP_PORT
		}
		range_from = (get_hash(me) + 1) % (2**160)
		range_to = get_hash(me)

	# Node that has to join the system
	else:
		me = {
			'ip': get_ip(),
			'port': sys.argv[1]
		}
		range_from = None
		range_to = get_hash(me)

	start_server = websockets.serve(handle_req, me['ip'], me['port'])
	print(f'IP:port {me}')
	print(f'ID: {get_hash(me)}')

	event_loop = asyncio.get_event_loop()
	event_loop.run_until_complete(start_server)

	if sys.argv[1] != 'bootstrap':
		event_loop.create_task(send_msg(BOOTSTRAP_URI, json.dumps({'query': 'join', 'new_peer': me})))

	event_loop.run_forever()
